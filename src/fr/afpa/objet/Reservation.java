package fr.afpa.objet;

import java.time.LocalDate;

public class Reservation {

	private int numeroChambre;
	private LocalDate debut;
	private LocalDate fin;
	private int duree;
	
	
	public Reservation(int numChambre, LocalDate arrivee_, LocalDate depart_) {
		setNumeroChambre(numChambre);
		debut = arrivee_;
		fin = depart_;
		duree = calculDureeSejour();
	}

	/**
	 * Calcule la duree du sejour a partir des dates de debut et de fin
	 * @return
	 */
	public int calculDureeSejour() {
		int i=0;
		LocalDate date = debut;
		while (!date.equals(fin)) {
			date = date.plusDays(1L);
			i++;
		}
		return i;
	}
	
	/**
	 * Calcul du cout du sejour
	 * @param chambres
	 * @return
	 */
	public float calculCout(Chambre[] chambres) {
		return duree * chambres[numeroChambre].getTypeDeChambre().getTarif();
	}
	
	/**
	 * Affichage du cout du sejour
	 * @param chambres
	 */
	public void affichageCout(Chambre[] chambres) {
		System.out.println("Le cout de la chambre "+numeroChambre
					+" est "+calculCout(chambres));
	}
	
	/**
	 * Retourne une chaine de caracteres avec les infos de 
	 * la reservation.
	 * @return
	 */
	public String infosReservation() {
		String info = "Chambre numero "+numeroChambre;
		info += "\nDate de sejour : "+debut+" - "+fin;
		return info;
	}

	/**
	 * Affichage des infos de la reservation.
	 */
	public void infos() {
		System.out.println(infosReservation());
	}
	
	
	public int getNumeroChambre() {
		return numeroChambre;
	}

	public void setNumeroChambre(int numeroChambre) {
		this.numeroChambre = numeroChambre;
	}

	public LocalDate getDebut() {
		return debut;
	}
	
	public void setDebut(LocalDate debut_) {
		debut = debut_;
	}


	public LocalDate getFin() {
		return fin;
	}


	public void setFin(LocalDate fin_) {
		fin = fin_;
	}


	public int getDuree() {
		return duree;
	}


	public void setDuree(int duree_) {
		duree = duree_;
	}
	
	
	
	
}
