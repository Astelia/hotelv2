package fr.afpa.objet;

public class Chambre {
	
	private int nbReservations;
	private int numeroChambre;
	private TypeChambre typeChambre;                                                                                                                                                                           
	private boolean libre;
	// attribut clients ayant reserves cette chambre
	private Client[] clients;
	
	public Chambre(int numChambre, TypeChambre typeChambre_) {
		nbReservations = 0;
		numeroChambre = numChambre;
		typeChambre = typeChambre_;
		libre = true;
		clients = new Client[5];
	}
	
	
	/**
	 * affichage detaille des informations d'une chambre 
	 */
	public void infos(Chambre[] chambres) {
		System.out.println("Chambre numero "+numeroChambre);
		System.out.println("Nombre de reservations pour cette chambre : "+nbReservations);
		System.out.println();
		typeChambre.infos();
		System.out.println();
		int i=0;
		while (i<clients.length && clients[i] != null) {
			clients[i].infos(chambres);
			i++;
		}
	}
	
	public int getNbReservations() {
		return nbReservations;
	}

	public void setNbReservations(int nbReserv) {
		nbReservations = nbReserv;
	}

	public void setNumeroChambre (int numchmb) {
		numeroChambre=numchmb;
	}
	
	public int getNumeroChambre () {
		return numeroChambre;
	}
	
	public void setTypeDeChambre (TypeChambre type) {
		typeChambre=type;
	}
	
	public TypeChambre getTypeDeChambre() {
		return typeChambre;
	}
	
	public void setLibre(boolean boo) {
		libre=boo;
	}
	
	public boolean getLibre() {
		return libre;
	}

	public Client[] getClients() {
		return clients;
	}

	public void setClients(Client[] clients_) {
		clients = clients_;
	}
	
	
}
