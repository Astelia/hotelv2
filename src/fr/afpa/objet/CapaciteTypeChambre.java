package fr.afpa.objet;

public class CapaciteTypeChambre {

	// attribut type de chambre (donnees provenant du fichier csv)
	private TypeChambre typeChambre;
	// attribut nombre de chambres de ce type dans un hotel
	private int nbMax;
	// attribut nombre de chambres reservees de ce type
	private int nbReservees;
	
	
	public CapaciteTypeChambre(TypeChambre type, int max) {
		typeChambre = type;
		nbMax = max;
		nbReservees = 0;
	}

	/**
	 * Retire 1 au nombre de chambres reservees de ce type
	 */
	public void liberationTypeChambre() {
		nbReservees -= 1;
	}
	
	/**
	 * Verifie si le nombre de reservations ne depassent pas le nombre
	 * de chambres minimum d'un certain type (c'est a dire 0).
	 * @return
	 */
	public boolean verifNbReservationsPositif() {
		return 0 < nbReservees;
	}
	
	/**
	 * Ajoute 1 au nombre de chambres reservees de ce type
	 */
	public void reservationTypeChambre() {
		nbReservees += 1;
	}
	
	/**
	 * Verifie si le nombre de reservations ne depassent pas le nombre
	 * de chambres max d'un certain type.
	 * @return
	 */
	public boolean verifNbReservationsMax() {
		return nbReservees < nbMax;
	}
	
	
	public TypeChambre getTypeChambre() {
		return typeChambre;
	}


	public void setTypeChambre(TypeChambre typeChambre_) {
		typeChambre = typeChambre_;
	}


	public int getNbMax() {
		return nbMax;
	}


	public void setNbMax(int nbMax_) {
		nbMax = nbMax_;
	}


	public int getNbReservees() {
		return nbReservees;
	}


	public void setNbReservees(int nbReservees_) {
		nbReservees = nbReservees_;
	}
	
	
	
}
