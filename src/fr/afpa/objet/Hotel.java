package fr.afpa.objet;

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

import fr.afpa.utilitaire.ControleDate;
import fr.afpa.utilitaire.ControleSaisie;
import fr.afpa.utilitaire.EnvoiMail;
import fr.afpa.utilitaire.LectureFichier;
import fr.afpa.utilitaire.Operation;
import fr.afpa.utilitaire.Question;

public class Hotel {

	private LocalDate dateDuJour;
	private Chambre[] chambres;
	private Client[] clients;
	private String[] employes;
	private CapaciteTypeChambre[] capaciteTypeChambres;
	private int nbDeChambre;
	private float chiffreAffaire;
	private Scanner in;
	
	public Hotel (LocalDate date, String chemin, Scanner scanner) {
		dateDuJour = date;
		capaciteTypeChambres = LectureFichier.traductionFichier(chemin);
		nbDeChambre = nbTotalChambres();
		chiffreAffaire = calculChiffreDAffaire();
		chambres = creationHotel();
		clients = new Client[5];
		
		employes = new String[1];
		employes[0] = "GH000";
		in = scanner;
	}
	
	/**
	 * Affichage du menu de l'hotel
	 */
	public void affichageMenuHotel() {
		System.out.println("-------------------  MENU HOTEL CDA JAVA -----------------------");
		System.out.println();
		System.out.println("A- Afficher l'etat de l'hotel");
		System.out.println("B- Afficher le nombre de chambre reservees");
		System.out.println("C- Afficher le nombre de chambre libres");
		System.out.println("D- Afficher le numero de la premiere chambre vide");
		System.out.println("E- Afficher le numero de la derniere chambre vide");
		System.out.println("F- Reserver une chambre");
		System.out.println("G- Liberer une chambre");
		System.out.println("H- Modifier une reservation");
		System.out.println("I- Annuler une reservation");
		System.out.println();
		System.out.println("Q- Quitter");
		System.out.println();
		System.out.println("-----------------------------------------------------------------");
		System.out.println();
		System.out.print("Votre choix : ");
	}
	
	/**
	 * Menu de l'hotel qui appelle les fonctions associees
	 */
	public void menuHotel() {
		affichageMenuHotel();
		String reponse = Question.scannerMot(in);
		switch (reponse) {
			case "A" : affichageChambres(listeChambresReservees()
							, "Liste des chambres reservees :");
					   break;
			case "B" : affichageNbChambresReservees();
					   break;
			case "C" : affichageNbChambresLibres();
					   break;
			case "D" : affichagePremiereChambre(true);
					   break;
			case "E" : affichageDerniereChambre(true);
					   break;
			case "F" : reserverChambre();
					   break;
			case "G" : // A completer
				   	   break;
			case "H" : // A completer
				   	   break;
			case "I" : annulerReservation();
				   	   break;
			default : System.out.println("Je ne comprends votre reponse.");
		}
		quitter(reponse);
	}
	
	/**
	 * Appelle la fonction menuHotel si la reponse n'est pas "Q"
	 * et met fin au programme sinon.
	 * @param reponse
	 */
	public void quitter(String reponse) {
		if (!reponse.equals("Q")) {
			menuHotel();
		}
	}
	
	public void annulerReservation() {
		System.out.print("Veuillez entrer votre nom : ");
		String nom = Question.scannerMot(in);
		System.out.print("Veuillez entrer votre prenom : ");
		String prenom = Question.scannerMot(in);
		if (clientExistsParNom(nom, prenom)) {
			Client client = trouverClientParNom(nom, prenom);
			int[] chambresReserveesParClient = trouverNumerosChambresParNomClient(nom, prenom);
			int numeroChambre = -1;
			System.out.print("Quelle chambre souhaitez-vous annuler parmi : |");
			for (int i=0; i<chambresReserveesParClient.length; i++) {
				System.out.print(chambresReserveesParClient[i]+" | ");
			}
			numeroChambre = Question.scannerEntier(in);
			if (indiceChambreAppartientAReservationClient(chambresReserveesParClient, numeroChambre)) {
				int i=0;
				while (i<client.getSejour().length && client.getSejour()[i].getNumeroChambre() != numeroChambre) {
					i++;
				}
				payerFacture(client, i, false);
				//libererChambre();
			}
			else {
				System.out.println("Vous n'avez pas reserver de chambre avec ce numero.");
				annulerReservation();
			}
		}
		else {
			System.out.println("Il n'y a aucune reservation a ce nom.");
		}
	}
	
	public void reserverChambre() {
		if (nbTypeChambresLibres() == 0) {
			System.out.println("L'hotel n'a plus de chambre disponible !");
		}
		else if (chambreParCriteresExist()) {
			System.out.println("Il n'y a plus de chambre disponible de ce type a ce jour.");
		}
		else {
			Client client = creationClient();
			ajoutReservationClient(client);
		}
	}
	
	// Ajoute une reservation au client s'il n'a pas plus de 4 reservations
	public void ajoutReservationClient(Client client) {
		if (verifNbReservationsMaxParClient(client)) {
			Reservation reservation = compareDatesSejour();
			int i = indicePremiereReservationVideDUnClient(client);
			client.getSejour()[i] = reservation;
			int numeroChambre = reservation.getNumeroChambre();
			chambres[numeroChambre].setNbReservations(chambres[numeroChambre].getNbReservations()+1);
			System.out.println("La chambre a ete reservee.");
			payerFacture(client, i, true);
		}
		else {
			System.out.println("Vous avez deja atteint le nombre de reservations max.");
		}
	}
	
	// Calcul somme a payer 
	public void payerFacture(Client client, int indiceReservation, boolean debit_credit) {
		float sommeAPayer = client.calculFactures(chambres)[indiceReservation];
		System.out.println("Vous devez payer "+sommeAPayer+" euros.");
		System.out.println("Veuillez entrer le numero de votre carte : ");
		String numeroCarte = Question.scannerMot(in);
		Operation operation = new Operation(dateDuJour, debit_credit, sommeAPayer, numeroCarte);
		System.out.println("Dans quel dossier stocker le fichier d'operation ? : ");
		String dossier = Question.scannerPhrase(in);
		operation.fichierOperation(dossier);
		int indiceChambre = client.getSejour()[indiceReservation].getNumeroChambre();
		creationPDFFactureEtEnvoie(dossier, client, indiceChambre);
	}
	
	// Creation du PDF avec l'envoi du mail
	public void creationPDFFactureEtEnvoie(String dossier, Client client, int indiceChambre) {
		String fileName = client.createInfosPdf(dossier, chambres, indiceChambre);
		EnvoiMail.envoyerMailSMTP(fileName, client.getMail());
	}
	
	// Verif nombre de reservations max par client
	public boolean verifNbReservationsMaxParClient(Client client) {
		return indicePremiereReservationVideDUnClient(client) < client.getSejour().length;
	}
	
	// Retourne l'indice du premier emplacement vide dans le tableau de reservation du client
	public int indicePremiereReservationVideDUnClient(Client client) {
		int i=0;
		while (i<client.getSejour().length && client.getSejour()[i] != null) {
			i++;
		}
		return i;
	}
	
	public Client creationClient() {
		System.out.print("Veuillez entrer le nom : ");
		String nom = Question.scannerMot(in);
		System.out.print("Veuillez entrer le prenom : ");
		String prenom = Question.scannerMot(in);
		if (clientExistsParNom(nom, prenom)) {
			Client client = trouverClientParNom(nom, prenom);
			return client;
		}
		else {
			System.out.print("Veuillez entrer votre mail : ");
			String mail = Question.scannerMail(in);
			System.out.println("Veuillez entrer le numero de votre carte : ");
			String numeroCarte = Question.scannerMot(in);
			Reservation[] reservs = {null, null, null, null, null};
			Client client = new Client(nom, prenom, mail, numeroCarte, genererLogin(), reservs);
			nouveauTableauClientss();
			clients[clients.length-1] = client;
			return client;
		}
	}
	
	// Genere un login et verifie qu'aucun autre client n'a le meme, si oui, regenere un autre
	// si non, retourne le login
	public String genererLogin() {
		String login = "";
		for (int i=0; i<10; i++) {
			login += aleatoireInferieur10();
		}
		if (!clientExistParLogin(login)) {
			return login;
		}
		else {
			return genererLogin();
		}
	}
	
	// Genere un nombre entier aleatoire inferieur a 10.
	public int aleatoireInferieur10() {
		return new Random().nextInt(10);
	}
	
	// Verifie que les dates de sejour n'entrent pas en conflit avec d'autres
	public boolean pasConflitDates(LocalDate debut, LocalDate fin) {
		return compareDatesSejour() != null;
	}
	
	// Compare la date de sejour entree avec les reservations deja effecutees par d'autres clients
	public Reservation compareDatesSejour() {
		LocalDate debut = Question.scannerDate(in);
		LocalDate fin = Question.scannerDate(in);
		if (ControleDate.dureeSejourValide(debut, fin)) {
			int indiceDansCTC = choisirIndiceChambreParCriteres();
			int indiceDansChambres = trouverIndiceChambreParCapaciteTC(indiceDansCTC);
		
			CapaciteTypeChambre cTChambre = choisirChambreParCriteres();
			int indiceMax = indiceDansChambres + (cTChambre.getNbMax() - cTChambre.getNbReservees());
			while (indiceDansChambres < indiceMax) {
				Chambre chambre = chambres[indiceDansChambres];
				int i=0;
				Client[] client = chambre.getClients();
				while (i<client.length) {
					Reservation reservation = trouverReservationParClientEtNumeroChambre(client[i], indiceDansChambres);
					if (ControleDate.pasZoneReservation(reservation.getDebut(), reservation.getFin(), debut)
							&& ControleDate.pasZoneReservation(reservation.getDebut(), reservation.getFin(), fin))  {
						return new Reservation(indiceDansChambres, debut, fin);
					}
				}
				indiceDansChambres++;
			}
		}
		else {
			System.out.println("Duree de sejour invalide ! Veuillez choisir d'autres dates.");
			compareDatesSejour();
		}
		return null;
	}
	
	public boolean indiceChambreAppartientAReservationClient(int[] numerosChambres, int numATester) {
		int i=0;
		while (i<numerosChambres.length) {
			if (numerosChambres[i] == numATester) {
				return true;
			}
		}
		return false;
	}
	
	public Reservation trouverReservationParClientEtNumeroChambre(Client client, int numeroChambre) {
		Reservation[] sejours = client.getSejour();
		int i=0;
		while (i<sejours.length && sejours[i].getNumeroChambre()!= numeroChambre) {
			 i++;
		}
		return sejours[i];
	}
		
	/**
	 * Calcule l'indice ou se trouve la chambre dans le tableau de chambres a partir
	 * de l'indice dans le tableau de capaciteTypeChambre.
	 * @param indice
	 * @return
	 */
	public int trouverIndiceChambreParCapaciteTC(int indice) {
		int res = 0;
		for (int i=0; i<indice; i++) {
			res += capaciteTypeChambres[i].getNbMax();
		}
		res += capaciteTypeChambres[indice].getNbReservees();
		return res;
	}
	
	/**
	 * Verifie si la chambre avec les criteres choisis existe
	 * @return
	 */
	public boolean chambreParCriteresExist() {
		return choisirChambreParCriteres() != null;
	}
	
	/**
	 * Retourne le type de chambre selon les criteres choisis par le client sinon null;
	 * @return
	 */
	public CapaciteTypeChambre choisirChambreParCriteres() {
		int indice = choisirIndiceChambreParCriteres();
		if (0<=indice && indice<capaciteTypeChambres.length) {
			return capaciteTypeChambres[indice-1];
		}
		else {
			return null;
		}
	}
	
	/**
	 * Retourne l'indice du type de chambre selon les criteres choisis par le client.
	 * @return
	 */
	public int choisirIndiceChambreParCriteres() {
		String[] infos = infosTypeChambres();
		int n = infos.length;
		boolean[] dispo = disponibiliteTypeChambres();
		System.out.println("Veuillez choisir le type de chambre que vous souhaitez parmi celles disponbles : ");
		for (int i=0; i<n; i++) {
			if (dispo[i]) {
				System.out.println((i+1)+" - "+infos[i]);
			}
		}
		System.out.print("Votre choix : ");
		int reponse = Question.scannerEntier(in);
		if (1<=reponse && reponse<=n && dispo[reponse-1]) {
			return reponse-1;
		}
		else {
			System.out.println("Reponse invalide");
			return choisirIndiceChambreParCriteres();
		}
	}	
	
	
	/**
	 * Retourne le nombre de types de chambres libres.
	 * @return
	 */
	public int nbTypeChambresLibres() {
		boolean[] dispo = disponibiliteTypeChambres();
		int nb=0;
		for (int i=0; i<capaciteTypeChambres.length; i++) {
			if (dispo[i]) {
				nb++;
			}
		}
		return nb;
	}
	
	/**
	 * Retourne un tableau de booleens :
	 * Si un type de chambre est disponible, alors la case correspondante est 
	 * a "true" sinon, a "false".
	 * @return
	 */
	public boolean[] disponibiliteTypeChambres() {
		boolean[] dispo = new boolean[capaciteTypeChambres.length];
		for (int i=0; i<capaciteTypeChambres.length; i++) {
			if (capaciteTypeChambres[i].getNbReservees() < capaciteTypeChambres[i].getNbMax()) {
				dispo[i] = true;
			}
		}
		return dispo;
	}
	
	/**
	 * Retourne le tableau des informations de chaque types de chambres.
	 * @return
	 */
	public String[] infosTypeChambres() {
		String[] res = new String[capaciteTypeChambres.length];
		for (int i=0; i<capaciteTypeChambres.length; i++) {
			String[] infos = capaciteTypeChambres[i].getTypeChambre().infosTypeChambre();
			for (int j=0; j<infos.length; j++) {
				res[i] += infos[j]+" ";
			}
		}
		return res;
	}
	
	// Chambre existe par numero dans reservations du client
	
	
	// Mot de passe code en dur ? Non dans un fichier a lire donc. A modifier.
	/**
	 * Verifie si le mot de passe entre dans la console est correct.
	 * @return
	 */
	public boolean verifMotDePasse() {
		System.out.println("Veuillez entrer le mot de passe : ");
		String mdp = Question.scannerMot(in);
		return mdp.equals("bidule");
	}
	
	/**
	 * Affichage menu authentification
	 */
	public static void affichageMenuAuthentif() {
		System.out.println("_________________________________________________");
		System.out.println();
		System.out.println("             Entrer votre login                  ");
		System.out.println();
		System.out.println("_________________________________________________");
	}
	
	/**
	 * Permet a l'utilisateur de s'authentifier.
	 * Affiche le menu en boucle si login non valide ou si
	 * le client ou l'employe n'existent pas.
	 */
	public void menuAuthentif() {
		affichageMenuAuthentif();
		String login = Question.scannerMot(in);
		if (ControleSaisie.isStringLoginClientValide(login)
				&& clientExistParLogin(login)) {
			trouverClientParLogin(login).infos(chambres);
		}
		else if (ControleSaisie.isStringLoginEmployeValide(login)
				&& employeExistParLogin(login)) {
			menuHotel();
		}
		else {
			System.out.println("Login incorrect !");
			menuAuthentif();
		}
	}
	
	/**
	 * Verifie si l'employe existe dans le tableau des employes
	 * @param login
	 * @return
	 */
	public boolean employeExistParLogin(String login) {
		int i=0;
		while (i<employes.length && !employes[i].contentEquals(login)) {
			i++;
		}
		return i != employes.length;
	}
	
	/**
	 * ajoute un employe dans le tableau des employes
	 * s'il n'existe pas.
	 * @param login
	 */
	public void ajouterEmploye(String login) {
		if (!employeExistParLogin(login)) {
			nouveauTableauEmployes();
			employes[employes.length-1] = login;
		}
		else {
			System.out.println("L'employe existe deja !");
		}
	}
	
	/**
	 * Remplace l'ancien tableau d'employes avec un nouveau
	 * tableau d'employes avec les memes valeurs mais, une case en plus.
	 */
	private void nouveauTableauEmployes() {
		String[] res = new String[employes.length + 1];
		for (int i=0; i<employes.length; i++) {
			res[i] = employes[i];
		}
		employes = res;
	}
	
	/**
	 * Verifie si le client existe dans le tableau de clients
	 * @param login
	 * @return
	 */
	public boolean clientExistParLogin(String login) {
		return trouverClientParLogin(login) != null;
	}
	
	/**
	 * Retourne le client si celui-ci est dans le tableau
	 * des clients et null s'il ne le trouve pas a partir du login
	 * @param login
	 * @return
	 */
	public Client trouverClientParLogin(String login) {
		int i=0;
		while (i<clients.length && !clients[i].getLogin().contentEquals(login)) {
			i++;
		}
		if (i == clients.length) {
			return null;
		}
		else {
			return clients[i];
		}
	}
	
	/**
	 * Remplace l'ancien tableau de clients avec un nouveau
	 * tableau de clients avec les memes valeurs mais, une case en plus.
	 */
	private void nouveauTableauClientss() {
		Client[] res = new Client[clients.length + 1];
		for (int i=0; i<clients.length; i++) {
			res[i] = clients[i];
		}
		clients = res;
	}
	
	/**
	 * Retourne la premiere chambre qui est libre si bool = true 
	 * et occupee sinon
	 * S'il ne touve pas, il retourne -1
	 * @param bool
	 * @return
	 */
	public int premiereChambre(boolean bool) {
		int i=0;
		while (i<chambres.length && chambres[i].getLibre() != bool) {
			i++;
		}
		if (i == chambres.length) {
			return -1;
		}
		return i;
	}
	
	/**
	 * Affiche la premiere chambre qui est libre si bool = true 
	 * et occupee sinon et indique aussi s'il n'y en a pas.
	 * @param bool
	 */
	public void affichagePremiereChambre(boolean bool) {
		String mot = "";
		if (bool) {
			mot = "vide";
		}
		else {
			mot = "occupee";
		}
		int i = premiereChambre(bool);
		if (i>=0) {
			System.out.println("La premiere chambre "+mot
					+" est la numero "+(i+1));
		}
		else {
			System.out.println("Il n'y a pas de chambre "+mot+".");
		}
	}
	
	/**
	 * Retourne la derniere chambre qui est libre si bool = true et occupee sinon
	 * S'il ne touve pas, il retourne -1
	 * @param bool
	 * @return
	 */
	public int derniereChambre(boolean bool) {
		int i=chambres.length-1;
		while (i>=0 && chambres[i].getLibre() != bool) {
			i--;
		}
		if (i == -1) {
			return -1;
		}
		return i;
	}
	
	/**
	 * Affiche la derniere chambre qui est libre si bool = true 
	 * et occupee sinon et indique aussi s'il n'y en a pas.
	 * @param bool
	 */
	public void affichageDerniereChambre(boolean bool) {
		String mot = "";
		if (bool) {
			mot = "vide";
		}
		else {
			mot = "occupee";
		}
		int i = derniereChambre(bool);
		if (i>=0) {
			System.out.println("La derniere chambre "+mot
						+" est la numero "+(i+1));
		}
		else {
			System.out.println("Il n'y a pas de chambre "+mot+".");
		}
	}
	
	/**
	 * Affiche les infos d'une liste de chambres libres
	 * ou occupees passees en parametre.
	 * @param liste
	 * @param libre_reservees
	 */
	public void affichageChambres(Chambre[] liste, String libre_reservees) {
		System.out.println(libre_reservees);
		for (int i=0; i<liste.length; i++) {
			liste[i].infos(liste);
		}
	}
	
	/**
	 * Calcul du chiffre d'affaire
	 * @return
	 */
	public float calculChiffreDAffaire() {
		float res = 0;
		for (int i=0; i<clients.length; i++) {
			res += clients[i].calculTotalFactures(chambres);
		}
		return res;
	}
	
	/**
	 * Cherche et retourne un tableau des numeros des chambres qu'un client
	 * a reserve a partir du nom de ce client.
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public int[] trouverNumerosChambresParNomClient(String nom, String prenom) {
		Client client = trouverClientParNom(nom, prenom);
		int[] res = new int[client.getSejour().length];
		for (int i=0; i<res.length; i++) {
			res[i] = client.getSejour()[i].getNumeroChambre();
		}
		return res;
	}
	
	/**
	 * Retourne vrai si une personne "nom", "prenom" existe dans le
	 * tableau des clients et faux sinon.
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public boolean clientExistsParNom(String nom, String prenom) {
		Client client = trouverClientParNom(nom, prenom);
		return client != null;
	}
	
	/**
	 * Cherche et retourne un client si celui-ci existe dans le
	 * tableau des clients via son nom et son prenom 
	 * et retourne null sinon.
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public Client trouverClientParNom(String nom, String prenom) {
		int i=0;
		while (i<clients.length && clients[i].getNom()!=nom && clients[i].getPrenom()!=prenom) {
			i++;
		}
		if (i==clients.length) {
			return null;
		}
		else {
			return clients[i];
		}
	}
	
	/**
	 * Retourne une chambre via son numero
	 * @param numero
	 * @return
	 */
	public Chambre trouverChambreParNumero(int numero) {
		return chambres[numero];
	}
	
	/**
	 * Cree la liste de chambres de l'hotel a partir des
	 * donnes contenues dans capaciteTypeChambres.
	 * @return
	 */
	private Chambre[] creationHotel() {
		int nb = nbTotalChambres();
		Chambre[] hotel = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<capaciteTypeChambres.length && j<nb) {
			for (int k=0; k<capaciteTypeChambres[i].getNbMax(); k++) {
				chambres[j] = new Chambre(j, capaciteTypeChambres[i].getTypeChambre());
				j++;
			}
			i++;
		}
		return hotel;
	}
	
	/**
	 * Calcul le nombre total de chambres a partir des
	 * donnes contenues dans capaciteTypeChambres.
	 * @return
	 */
	public int nbTotalChambres() {
		int res = 0;
		for (int i=0; i<capaciteTypeChambres.length; i++) {
			res += capaciteTypeChambres[i].getNbMax();
		}
		return res;
	}
	
	/**
	 * Retourne la liste des chambres libres de l'hotel.
	 * @return
	 */
	public Chambre[] listeChambresLibres() {
		int nb = nbChambresLibres();
		Chambre[] listeChambres = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<listeChambres.length && j<nbDeChambre) {
			if (chambres[j].getLibre()) {
				listeChambres[i] = chambres[j];
				i++;
			}
			j++;
		}
		return listeChambres;
	}
	
	/**
	 * Retourne la liste des chambres reservees de l'hotel.
	 * @return
	 */
	public Chambre[] listeChambresReservees() {
		int nb = nbChambresReservees();
		Chambre[] listeChambres = new Chambre[nb];
		int i=0;
		int j=0;
		while (i<listeChambres.length && j<nbDeChambre) {
			if (!chambres[j].getLibre()) {
				listeChambres[i] = chambres[j];
				i++;
			}
			j++;
		}
		return listeChambres;
	}
	
	/**
	 * Retourne le nombre de chambres  libres dans l'hotel.
	 * @return
	 */
	public int nbChambresLibres() {
		int res = 0;
		for (int i=0; i<chambres.length; i++) {
			if (chambres[i].getLibre()) {
				res++;
			}
		}
		return res;
	}
	
	/**
	 * Affiche le nombre de chambres libres dans l'hotel.
	 */
	public void affichageNbChambresLibres() {
		System.out.println("Il y a "+nbChambresLibres()+" chambres libres dans l'hotel.\n");
	}
	
	/**
	 * Retourne le nombre de chambres reservees dans l'hotel.
	 * @return
	 */
	public int nbChambresReservees() {
		int res = 0;
		for (int i=0; i<chambres.length; i++) {
			if (!chambres[i].getLibre()) {
				res++;
			}
		}
		return res;
	}
	
	/**
	 * Affiche le nombre de chambres reservees dans l'hotel.
	 */
	public void affichageNbChambresReservees() {
		System.out.println("Il y a "+nbChambresReservees()+" chambres reservees dans l'hotel.\n");
	}
	
	
	public LocalDate getDateDuJour() {
		return dateDuJour;
	}

	public void setDateDuJour(LocalDate date) {
		dateDuJour = date;
	}

	public Chambre[] getTableauDeChambre() {
		return chambres;
	}

	public void setTableauDeChambre(Chambre[] tableauDeChambre_) {
		chambres = tableauDeChambre_;
	}

	public Client[] getClients() {
		return clients;
	}


	public void setClients(Client[] clients_) {
		clients = clients_;
	}


	public CapaciteTypeChambre[] getCapaciteTypeChambres() {
		return capaciteTypeChambres;
	}


	public void setTypeChambres(CapaciteTypeChambre[] capaciteTypeChambres_) {
		capaciteTypeChambres = capaciteTypeChambres_;
	}


	public int getNombreDeChambre() {
		return nbDeChambre;
	}

	public void setNombreDeChambre(int nombreDeChambre_) {
		nbDeChambre = nombreDeChambre_;
	}

	public float getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(float chiffreAffaire_) {
		chiffreAffaire = chiffreAffaire_;
	}
		

	
}
