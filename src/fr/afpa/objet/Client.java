package fr.afpa.objet;

import com.itextpdf.layout.element.Paragraph;

import fr.afpa.utilitaire.PDFGenerator;

public class Client {

	private String nom;
	private String prenom;
	private String mail;
	private String numeroCarte;
	// numero de reservation qui sert aussi de login
	private String login;
	private Reservation[] sejours;
	
	
	public Client(String nom_, String prenom_, String mail_, String numeroCarte_, String login_, Reservation[] sejour_) {
		nom = nom_;
		prenom = prenom_;
		mail = mail_;
		numeroCarte = numeroCarte_;
		login = login_;
		sejours = sejour_;
	}
	
	public boolean reservationsPleines() {
		int cpt = 0;
		for (int i=0; i<sejours.length; i++) {
			if (sejours[i] == null) {
				cpt++;
			}
		}
		return cpt == 0;
	}
	
	/**
	 * Creation du fichier PDF qui contiendra la facture
	 * Elle prend en parametre le chemin du dossier qui contiendra le fichier
	 * et la liste des chambres de l'hotel ou reside le client.   
	 * @param dossier
	 * @param chambres
	 */
	public String createInfosPdf(String dossier, Chambre[] chambres, int indiceChambre) {
		String fileName = dossier+nom+prenom+login+".pdf";
		PDFGenerator.createPDF(fileName, createParagraphe(chambres, indiceChambre));
		return fileName;
	}
	
	/**
	 * Creation du paragraphe de la facture pour toutes les reservations pour creer le pdf prend en
	 * parametre la liste des chambres de l'hotel ou reside le client.
	 * @param chambres
	 * @return
	 */
	private Paragraph createParagraphe(Chambre[] chambres, int indiceChambre) {
		Paragraph paragraphe = new Paragraph();
		String[] infos = infosClientUneReservation(chambres, indiceChambre);
		for (int i=0; i<infos.length; i++) {
			paragraphe.add(infos[i]);
		}
		return paragraphe;
	}
	
	// Mettre aussi details type de chambre ?
	/**
	 * Retourne un tableau des infos du client sans son numero de carte
	 * @param chambres
	 * @return
	 */
	public String[] infosClientTotalReservations(Chambre[] chambres) {
		String[] res = new String[5+sejours.length];
		res[0] = "Nom : "+nom+" "+prenom;
		res[1] = "Email : "+mail;
		res[2] = "Login : "+login;
		res[3] = "Reservation(s) : ";
		int i=0;
		while (i<sejours.length && sejours[i]!=null) {
			res[i+4] = sejours[i].infosReservation();
			i++;
		}
		res[res.length-1] = "Cout total : "+calculTotalFactures(chambres);
		return res;
	}
	
	public String[] infosClientUneReservation(Chambre[] chambres, int indiceChambre) {
		String[] res = new String[5+sejours.length];
		res[0] = "Nom : "+nom+" "+prenom;
		res[1] = "Email : "+mail;
		res[2] = "Login : "+login;
		res[3] = "Reservation(s) : ";
		int i=0;
		while (i<sejours.length && sejours[i]!=null) {
			res[i+4] = sejours[i].infosReservation();
			i++;
		}
		res[res.length-1] = "Cout total : "+calculFactures(chambres)[indiceChambre];
		return res;
	}
	
	/**
	 * Calcul du montant de la facture 
	 * @param chambres
	 * @return
	 */
	public float[] calculFactures(Chambre[] chambres) {
		float[] res = new float[sejours.length];
		for (int i=0; i<sejours.length; i++) {
			res[i] = sejours[i].calculCout(chambres);
		}
		return res;
	}
	
	// Total de toutes les factures
	public float calculTotalFactures(Chambre[] chambres) {
		float total = 0;
		float[] factures = calculFactures(chambres);
		for (int i=0; i<factures.length; i++) {
			total += factures[i];
		}
		return total;
	}
	
	/**
	 * Affichage des infos du client sans son numero de carte
	 * @param chambres
	 */
	public void infos(Chambre[] chambres) {
		for (int i=0; i<infosClientTotalReservations(chambres).length; i++) {
			System.out.println(infosClientTotalReservations(chambres)[i]);
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom_) {
		nom = nom_;
	}


	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom_) {
		prenom = prenom_;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail_) {
		mail = mail_;
	}

	public String getNumeroCarte() {
		return numeroCarte;
	}

	public void setNumeroCarte(String numeroCarte_) {
		numeroCarte = numeroCarte_;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login_) {
		login = login_;
	}

	public Reservation[] getSejour() {
		return sejours;
	}

	public void setSejour(Reservation[] sejour_) {
		sejours = sejour_;
	}
	
	
	
	
}
