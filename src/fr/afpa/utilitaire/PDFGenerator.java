package fr.afpa.utilitaire;

import java.io.FileNotFoundException;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

public class PDFGenerator {

	/**
	 * Cree un fichier pdf a partir d'un chemin de fichier
	 * et d'un paragraphe de type Paragraph.
	 * @param filename
	 * @param paragraphe
	 */
	public static void createPDF(String filename, Paragraph paragraphe) {
		try {
			PdfWriter writer = new PdfWriter(filename);
			PdfDocument pdf = new PdfDocument(writer);
			Document document = new Document(pdf, PageSize.A4);
			document.add(paragraphe);
			document.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier est-il ferme ?");
		}
	}
	
}
