package fr.afpa.utilitaire;

import java.time.LocalDate;

public class Operation {
	
	private LocalDate date;
	private boolean nature;
	private float valeur;
	private String numeroCarte;
	
	
	public Operation(LocalDate date_, boolean nature_, float valeur_, String numero) {
		date = date_;
		nature = nature_;
		valeur = valeur_;
		numeroCarte = numero;
	}


	/**
	 * Affichage des informations relatives a la transaction
	 * bancaire.
	 */
	public void infos() {
		String[] texte = texte();
		for (int i=0; i<texte.length; i++) {
			System.out.println(texte[i]);
		}
	}
	
	/**
	 * Retourne un tableau de chaines de caracteres des informations relatives 
	 * a la transaction bancaire.
	 * @return
	 */
	public String[] texte() {
		String ligne1 = "Date de l'operation : "+date;
		String ligne2 = "Nature de l'operation : ";
		if (nature) {
			ligne2 += "credit";
		}
		else {
			ligne2 += "debit";
		}
		String ligne3 = "Valeur : "+valeur;
		String ligne4 = "Numero de carte : "+numeroCarte;
		String[] res = {ligne1, ligne2, ligne3, ligne4};
		return res;
	}
	
	/**
	 * Cree dans le dossier specifie un fichier relatant la transaction bancaire
	 * @param dossier
	 */
	public void fichierOperation(String dossier) {
		String fileName = dossier+date.toString()+".txt";
		EcritureFichier.transactionBancaire(fileName, texte());
	}
	
	
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date_) {
		date = date_;
	}

	public boolean isNature() {
		return nature;
	}

	public void setNature(boolean nature_) {
		nature = nature_;
	}

	public float getValeur() {
		return valeur;
	}

	public void setValeur(float valeur_) {
		valeur = valeur_;
	}

	public String getNumeroCarte() {
		return numeroCarte;
	}

	public void setNumeroCarte(String numeroCarte_) {
		numeroCarte = numeroCarte_;
	}
	
	
	

}
